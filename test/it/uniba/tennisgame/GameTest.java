package it.uniba.tennisgame;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GameTest {

	@Test
	void testFifteenThirty() throws Exception {
		// Arrange - Create a new game: 
		Game game = new Game("Federer", "Nadal");
		
		// Act - Get the name of the two players: 
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		// Get the status of the game:
		String status = game.getGameStatus();
		
		// Assert
		assertEquals( "Federer fifteen - Nadal thirty", status );
	}

	@Test
	void testFortyThirty() throws Exception {
		// Arrange - Create a new game: 
		Game game = new Game("Federer", "Nadal");
		
		// Act - Get the name of the two players: 
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		// Get the status of the game:
		String status = game.getGameStatus();
		
		// Assert
		assertEquals( "Federer forty - Nadal thirty", status );
	}
	
	@Test
	void testPlayerOneWins() throws Exception {
		// Arrange - Create a new game: 
		Game game = new Game("Federer", "Nadal");
		
		// Act - Get the name of the two players: 
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName1);
		
		
		// Get the status of the game:
		String status = game.getGameStatus();
		
		// Assert
		assertEquals( "Federer wins", status );
	}
	
	@Test
	void testFifteenForty() throws Exception {
		// Arrange - Create a new game: 
		Game game = new Game("Federer", "Nadal");
		
		// Act - Get the name of the two players: 
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		
		// Get the status of the game:
		String status = game.getGameStatus();
		
		// Assert
		assertEquals( "Federer fifteen - Nadal forty", status );
	}
	
	@Test
	void testDeuce() throws Exception {
		// Arrange - Create a new game: 
		Game game = new Game("Federer", "Nadal");
		
		// Act - Get the name of the two players: 
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		
		// Get the status of the game:
		String status = game.getGameStatus();
		
		// Assert
		assertEquals( "Deuce", status );
	}
	
	@Test
	void testAdvantagePlayerOne() throws Exception {
		// Arrange - Create a new game: 
		Game game = new Game("Federer", "Nadal");
		
		// Act - Get the name of the two players: 
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName1);
		
		
		// Get the status of the game:
		String status = game.getGameStatus();
		
		// Assert
		assertEquals( "Advantage Federer", status );
	}
	
	@Test
	void testPlayerTwoWins() throws Exception {
		// Arrange - Create a new game: 
		Game game = new Game("Federer", "Nadal");
		
		// Act - Get the name of the two players: 
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		
		// Get the status of the game:
		String status = game.getGameStatus();
		
		// Assert
		assertEquals( "Nadal wins", status );
	}
	
	@Test
	void testAdvantagePlayerTwo() throws Exception {
		// Arrange - Create a new game: 
		Game game = new Game("Federer", "Nadal");
		
		// Act - Get the name of the two players: 
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		
		// Get the status of the game:
		String status = game.getGameStatus();
		
		// Assert
		assertEquals( "Advantage Nadal", status );
	}
	
	@Test
	void testDeuce2() throws Exception {
		// Arrange - Create a new game: 
		Game game = new Game("Federer", "Nadal");
		
		// Act - Get the name of the two players: 
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		
		
		// Get the status of the game:
		String status = game.getGameStatus();
		
		// Assert
		assertEquals( "Deuce", status );
	}
}


