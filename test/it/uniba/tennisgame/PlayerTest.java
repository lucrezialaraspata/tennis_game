package it.uniba.tennisgame;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PlayerTest {

	@Test
	public void scoreShouldBeIncreased() {
		// Arrange
		Player player = new Player( "Federer", 0 );
		
		// Act
		player.incrementScore();
				
		// Assert
		assertEquals( 1, player.getScore() );
	}
	
	@Test
	public void scoreShouldNotBeIncreased() {
		// Arrange
		Player player = new Player( "Federer", 0 );
		
		// Act
				
		// Assert
		assertEquals( 0, player.getScore() );
	}
	
	@Test
	public void scoreShoulBeLove() {
		// Arrange
		Player player = new Player( "Federer", 0 );
		
		// Act
		String stringScore = player.getScoreAsString();
				
		// Assert
		assertEquals( "love", stringScore );
	}
	
	@Test
	public void scoreShoulBeFifteen() {
		// Arrange
		Player player = new Player( "Federer", 1 );
		
		// Act
		String stringScore = player.getScoreAsString();
				
		// Assert
		assertEquals( "fifteen", stringScore );
	}
	
	
	@Test
	public void scoreShoulBeThirty() {
		// Arrange
		Player player = new Player( "Federer", 2 );
		
		// Act
		String stringScore = player.getScoreAsString();
				
		// Assert
		assertEquals( "thirty", stringScore );
	}
	
	
	@Test
	public void scoreShoulBeForty() {
		// Arrange
		Player player = new Player( "Federer", 3 );
		
		// Act
		String stringScore = player.getScoreAsString();
				
		// Assert
		assertEquals( "forty", stringScore );
	}
	
	@Test
	public void scoreShoulBeNullIfNegative() {
		// Arrange
		Player player = new Player( "Federer", -1 );
		
		// Act
		String stringScore = player.getScoreAsString();
				
		// Assert
		assertNull( stringScore );
	}
	
	@Test
	public void scoreShoulBeNullIfMoreThanThree() {
		// Arrange
		Player player = new Player( "Federer", 4 );
		
		// Act
		String stringScore = player.getScoreAsString();
				
		// Assert
		assertNull( stringScore );
	}
	
	
	@Test
	public void shouldBeTie() {
		// Arrange
		Player player1 = new Player( "Federer", 3 );
		Player player2 = new Player( "Nadal", 3 );
		
		// Act
		boolean isTie = player1.isTieWith( player2 );
				
		// Assert
		assertTrue( isTie );
	}
	
	@Test
	public void shouldNotBeTie() {
		// Arrange
		Player player1 = new Player( "Federer", 1 );
		Player player2 = new Player( "Nadal", 3 );
		
		// Act
		boolean isTie = player1.isTieWith( player2 );
				
		// Assert
		assertFalse( isTie );
	}
	
	@Test
	public void shouldHaveAtLeastFortyPoints() {
		// Arrange
		Player player1 = new Player( "Federer", 3 );
		
		// Act
		boolean leastForty = player1.hasAtLeastFortyPoints();
				
		// Assert
		assertTrue( leastForty );
	}
	
	
	@Test
	public void shouldNotHaveAtLeastFortyPoints() {
		// Arrange
		Player player1 = new Player( "Federer", 2 );
		
		// Act
		boolean notleastForty = player1.hasAtLeastFortyPoints();
				
		// Assert
		assertFalse( notleastForty );
	}

	@Test
	public void shouldNotHaveLessThanFortyPoints() {
		// Arrange
		Player player1 = new Player( "Federer", 3 );
		
		// Act
		boolean notLessForty = player1.hasLessThanFortyPoints();
				
		// Assert
		assertFalse( notLessForty );
	}
	
	@Test
	public void shouldHaveLessThanFortyPoints() {
		// Arrange
		Player player1 = new Player( "Federer", 2 );
		
		// Act
		boolean lessForty = player1.hasLessThanFortyPoints();
				
		// Assert
		assertTrue( lessForty );
	}
	
	@Test
	public void shouldHaveMoreThanFortyPoints() {
		// Arrange
		Player player1 = new Player( "Federer", 4 );
		
		// Act
		boolean moreForty = player1.hasMoreThanFourtyPoints();
				
		// Assert
		assertTrue( moreForty );
	}
	
	@Test
	public void shouldNotHaveMoreThanFortyPoints() {
		// Arrange
		Player player1 = new Player( "Federer", 2 );
		
		// Act
		boolean notMoreForty = player1.hasMoreThanFourtyPoints();
				
		// Assert
		assertFalse( notMoreForty );
	}
	
	@Test
	public void shouldHaveOnePointAdvantage() {
		// Arrange
		Player player1 = new Player( "Federer", 3 );
		Player player2 = new Player( "Nadal", 2 );
		
		// Act
		boolean isAdvantaged = player1.hasOnePointAdvantageOn( player2 );
				
		// Assert
		assertTrue( isAdvantaged );
	}
	
	@Test
	public void shouldNotHaveOnePointAdvantage() {
		// Arrange
		Player player1 = new Player( "Federer", 1 );
		Player player2 = new Player( "Nadal", 2 );
		
		// Act
		boolean isAdvantaged = player1.hasOnePointAdvantageOn( player2 );
				
		// Assert
		assertFalse( isAdvantaged );
	}
	
	
	@Test
	public void shouldHaveAtLeastTwoPointsAdvantage() {
		// Arrange
		Player player1 = new Player( "Federer", 4 );
		Player player2 = new Player( "Nadal", 2 );
		
		// Act
		boolean isAdvantaged = player1.hasAtLeastTwoPointsAdvantageOn( player2 );
				
		// Assert
		assertTrue( isAdvantaged );
	}
	
	@Test
	public void shouldNotHaveAtLeastTwoPointsAdvantage() {
		// Arrange
		Player player1 = new Player( "Federer", 1 );
		Player player2 = new Player( "Nadal", 2 );
		
		// Act
		boolean isAdvantaged = player1.hasAtLeastTwoPointsAdvantageOn( player2 );
				
		// Assert
		assertFalse( isAdvantaged );
	}
}
